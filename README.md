# Nathan & Tom compiler

## APP4 Info 2020-2021

### Lancer le compilateur

> ⚠ Les commandes sont à executer depuis la racine du projet.

Commande pour compiler et executer un fichier source :

```bash
java -jar target/mvnCompiler-1.0.0.jar pathToSource pathToOutput
```
*pathToSource* => le chemin vers le fichier source

*pathToOutput* => le chemin optionnel vers le fichier de sortie ("./generatedCode" par défaut)

Exemple d'utilisation avec un de nos tests fourni :
```bash
java -jar target/mvnCompiler-1.0.0.jar src/test/test/test_addition
```
> En cas de modification du code source : la commande `mvn clean install` doit être exécutée.

### Lancer nos tests

Commande pour compiler et executer tous les tests natifs fournis avec le compilateur :
```bash
java -jar target/mvnCompiler-1.0.0.jar --test pathToMsm
```
*pathToMsm* => le chemin vers le binaire msm

**test_main** échoue pour un bonne raison: il s'agit d'un test sur les variables globales.

### Ajouter un test

Pour ajouter un test **testName** aux tests natifs fournis avec le compilateur :
Ajouter le fichier source du test **test_testName** dans `src/test/test` et un fichier texte qui contient les résultats attendus **result_testName** dans `src/test/result_test`.
> ⚠ Chaque test_testName doit avoir son result_TestName correspondant dans le dossier result_test.

