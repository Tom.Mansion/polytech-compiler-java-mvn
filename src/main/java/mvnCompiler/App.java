package mvnCompiler;

import java.io.BufferedReader;
import java.io.File; // Import the File class
import java.io.FileNotFoundException; // Import this class to handle errors
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; // Import the Scanner class to read text files

public class App {

	// VM : https://perso.limsi.fr/lavergne/app4-compil/
	// commande à effectuer : mvn clean install && java -jar
	// target/mvnCompiler-1.0-SNAPSHOT.jar*
	// msm generatedCode

	public static void main(String[] args) {

		System.out.println("  Nathan & Tom compiler == APP4 Info == 2020-2021");

		if (args.length == 0) {
			System.out.println("No parameters provided");
			System.out.println("Use --test pathToMsmBinary to test our test files");
			System.out.println("Use a path to compile your code");
			return;
		}

		if (args[0].equals("--test")) {
			// Run tests

			if (args.length < 2) {
				System.out.println("No msm path provided");
				System.out.println("Please provide a path to your msm binary file");
			} else {
				testAll(args[1]);
			}
		} else {
			// Run compiler from file
			List<String> code = read(args[0]);
			String outPath = "generatedCode";
			if (args.length >= 2)
				outPath = args[1];

			compile(code, true, outPath);

			if (args.length < 2) {
				System.out.println("No output path provided");
				System.out.println("The default output file will be ./generatedCode");
			}
		}

	}

	private static void testAll(String pathToMsm) {
		// Load all tests :
		System.out.println("Loading tests");
		File f = new File("src/test/test");
		String[] tests = f.list();

		// For each pathname in the pathnames array
		for (String test : tests) {
			// Print the names of files and directories
			System.out.println("\n" + test);
			List<String> code = read("src/test/test/" + test);
			if (compile(code, false, "generatedTestCode")) {
				// Compilation succedded
				// Run msm with the generated code
				ProcessBuilder processBuilder = new ProcessBuilder();
				processBuilder.command("bash", "-c", pathToMsm + " ./generatedTestCode");
				try {

					Process process = processBuilder.start();

					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

					int exitVal = process.waitFor();
					if (exitVal == 0) {

						String resultsFile = "result_" + test.split("_")[1];
						List<String> results = read("src/test/result_test/" + resultsFile);

						if (results == null) {
							System.err.println(
									"No given results for the test " + test + " ( src/test/result_test/" + resultsFile + " not found)");
						} else {
							String line;
							int nbLine = 0;
							while ((line = reader.readLine()) != null) {
								if (nbLine >= results.size()) {
									System.err.println("Wrong answer at line " + nbLine);
									System.err.println("'" + line + "' is not ''");
								} else if (!line.equals(results.get(nbLine))) {
									System.err.println("Wrong answer at line " + nbLine);
									System.err.println("'" + line + "' is not '" + results.get(nbLine) + "'");
								}
								nbLine++;
							}
						}
					} else {
						System.err.println("An error occured while executing the code");
					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} else {
				// compilation failed
				System.out.println("Failed");
			}
		}
	}

	private static Boolean compile(List<String> code, boolean debug, String outputPath) {
		try {

			// Runtime
			List<String> runtime = read("runTime");
			List<String> newList = new ArrayList<String>(runtime);

			// User code
			newList.addAll(code);

			if (debug)
				System.out.println("   Code lexical analysis");
			analyseLexicale al = new analyseLexicale(newList);
			if (debug) {
				// Display tokens
				for (Token t : al.tokenList)
					System.out.print(t.chainValue + " ");
				System.out.println(al.tokenList.size() + " Token");
			}

			if (debug)
				System.out.println("   Code syntexical analysis");
			analyseSyntaxique as = new analyseSyntaxique(al);

			Noeud tree = as.createTree();
			new analyseSementique(tree);

			if (debug)
				as.displayTree(tree, 0, true);

			codeGenerator cg = new codeGenerator(tree);

			// Wrinting file
			write(cg.smCode, outputPath);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static List<String> read(String pathFile) {
		List<String> importData = new ArrayList<String>();
		try {
			File myObj = new File(pathFile);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String line = myReader.nextLine();
				importData.add(line);
			}
			myReader.close();
			return importData;
		} catch (FileNotFoundException e) {
			System.out.println("File '" + pathFile + "' Not found");
			e.printStackTrace();
		}
		return null;
	}

	private static void write(String str, String fileName) {
		FileWriter myWriter;
		try {
			myWriter = new FileWriter(fileName);
			myWriter.write(str);
			myWriter.close();
		} catch (IOException e) {
			System.out.println("Error while writing to the file.");
			e.printStackTrace();
		}
	}

}
