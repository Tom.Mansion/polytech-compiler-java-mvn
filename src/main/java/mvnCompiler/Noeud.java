package mvnCompiler;

import java.util.ArrayList;
import java.util.List;

enum NoeudType {
	// Operations
	addition, incrementation, soustraction, decrementation, multiplication, division, modulo, puissance, inferieur,
	inferieuregal, superieur, superieuregal, different, egal, et, ou, parouvrante, parfermante, pointvirgule, not,
	soustrationU,
	// Nombres et variables
	constant, identificator, attribution, declaration, appel, indirection,
	// conditionnelles
	loop, cond, loopBreak,
	// tête instructions
	block, test, drop, debug, function, retour, send, receive,

	// Pointeurs
	Reference,
}

public class Noeud {
	NoeudType type; // différent du type des token
	String chainValue;
	List<Noeud> childrens;
	int wholeValue;
	int lineNum;
	int collum;

	int slot; // for variables positions, given by semetical analysis
	int nbSlot; // for functions requiered memory

	public Noeud(NoeudType type, int lineNum, int collum) {
		this.type = type;
		this.lineNum = lineNum;
		this.collum = collum;
		childrens = new ArrayList<Noeud>();

	}

	void addChildren(Noeud child) {
		this.childrens.add(child);
	}
}
