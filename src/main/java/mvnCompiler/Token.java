package mvnCompiler;

enum TokenType {
	// operations
	addition, incrementation, soustraction, decrementation, multiplication, division, modulo, puissance, inferieur,
	inferieuregal, superieur, superieuregal, different, egal, et, ou, attribution, parouvrante, parfermante, pointvirgule,
	virgule, accOuvrante, accFermante, crochetOuvrant, crochetFermant,
	// Operateurs unaires
	not, negatif,
	// nombres et variables
	type, constant, identificator,
	// conditionnelles
	condIf, condElse, condFor, condWhile,
	// Autres
	debug, send, receive, retour, EOF, loopBreak

}

public class Token {
	public TokenType type;
	public String chainValue;
	public int wholeValue;
	public int lineNum;
	public int collum;

	public Token(TokenType type, String chainValue, int wholeValue, int lineNum, int collum) {
		this.type = type;
		this.chainValue = chainValue;
		this.wholeValue = wholeValue;
		this.lineNum = lineNum + 1;
		this.collum = collum + 1;
	}

}
