package mvnCompiler;

import java.util.ArrayList;
import java.util.List;

public class analyseLexicale {
	int cursor;
	List<Token> tokenList;

	// Constructor
	analyseLexicale(List<String> code) throws Exception {

		this.tokenList = this.analyse(code);

		this.cursor = 0;
	}

	// Analyse
	public List<Token> analyse(List<String> code) throws Exception {
		List<Token> tokenList = new ArrayList<Token>();
		int nbLigne = 0;
		while (nbLigne < code.size()) {
			int cursor = 0;
			while (cursor < code.get(nbLigne).length()) {
				// TODO : test range cursor + 1
				switch (code.get(nbLigne).charAt(cursor)) {
					case '+':
						if (code.get(nbLigne).charAt(cursor + 1) == '+') {
							tokenList.add(new Token(TokenType.incrementation, "++", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.addition, "+", 0, nbLigne, cursor));
						}
						break;
					case '-':
						if (code.get(nbLigne).charAt(cursor + 1) == '-') {
							tokenList.add(new Token(TokenType.decrementation, "--", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.soustraction, "-", 0, nbLigne, cursor));
						}
						break;
					case '*':
						tokenList.add(new Token(TokenType.multiplication, "*", 0, nbLigne, cursor));
						break;
					case '/':
						tokenList.add(new Token(TokenType.division, "/", 0, nbLigne, cursor));
						break;
					case '%':
						tokenList.add(new Token(TokenType.modulo, "%", 0, nbLigne, cursor));
						break;
					case '^':
						tokenList.add(new Token(TokenType.puissance, "^", 0, nbLigne, cursor));
						break;
					case '>':
						if (code.get(nbLigne).charAt(cursor + 1) == '=') {
							tokenList.add(new Token(TokenType.superieuregal, ">=", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.superieur, ">", 0, nbLigne, cursor));
						}
						break;
					case '<':
						if (code.get(nbLigne).charAt(cursor + 1) == '=') {
							tokenList.add(new Token(TokenType.inferieuregal, "<=", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.inferieur, "<", 0, nbLigne, cursor));
						}
						break;
					case '=':
						if (code.get(nbLigne).charAt(cursor + 1) == '=') {
							tokenList.add(new Token(TokenType.egal, "==", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.attribution, "=", 0, nbLigne, cursor));
						}
						break;
					case '(':
						tokenList.add(new Token(TokenType.parouvrante, "(", 0, nbLigne, cursor));
						break;
					case ')':
						tokenList.add(new Token(TokenType.parfermante, ")", 0, nbLigne, cursor));
						break;
					case '{':
						tokenList.add(new Token(TokenType.accOuvrante, "{", 0, nbLigne, cursor));
						break;
					case '}':
						tokenList.add(new Token(TokenType.accFermante, "}", 0, nbLigne, cursor));
						break;
					case ';':
						tokenList.add(new Token(TokenType.pointvirgule, ";", 0, nbLigne, cursor));
						break;
					case ',':
						tokenList.add(new Token(TokenType.virgule, ",", 0, nbLigne, cursor));
						break;
					case '&':
						if (code.get(nbLigne).charAt(cursor + 1) == '&') {
							tokenList.add(new Token(TokenType.et, "&&", 0, nbLigne, cursor));
							cursor++;
						}
						break;
					case '|':
						if (code.get(nbLigne).charAt(cursor + 1) == '|') {
							tokenList.add(new Token(TokenType.ou, "||", 0, nbLigne, cursor));
							cursor++;
						}
						break;
					case '!':
						if (code.get(nbLigne).charAt(cursor + 1) == '=') {
							tokenList.add(new Token(TokenType.different, "!=", 0, nbLigne, cursor));
							cursor++;
						} else {
							tokenList.add(new Token(TokenType.not, "!", 0, nbLigne, cursor));
						}
						break;

					default:
						if (Character.isDigit(code.get(nbLigne).charAt(cursor))) {
							// Number

							int val = 0;

							while (Character.isDigit(code.get(nbLigne).charAt(cursor))) {
								val = (val * 10) + Integer.parseInt(String.valueOf(code.get(nbLigne).charAt(cursor)));
								cursor++;
							}
							cursor--;

							tokenList.add(new Token(TokenType.constant, "tkn-const", val, nbLigne, cursor));

						} else {
							if (Character.isLetter(code.get(nbLigne).charAt(cursor))) {

								// char c = code.get(nbLigne).charAt(cursor);
								// tokenList.add(new Token(TokenType.identificator, String.valueOf(c), 0,
								// nbLigne, cursor));

								String ident = "";

								while (Character.isLetter(code.get(nbLigne).charAt(cursor)) || code.get(nbLigne).charAt(cursor) == '_' ) {
									ident += code.get(nbLigne).charAt(cursor);
									cursor++;
								}
								cursor--;

								// Find token
								switch (ident) {
									case "debug":
										tokenList.add(new Token(TokenType.debug, "dbg", 0, nbLigne, cursor));
										break;
									case "return":
										tokenList.add(new Token(TokenType.retour, "retour", 0, nbLigne, cursor));
										break;
									case "if":
										tokenList.add(new Token(TokenType.condIf, "condIf", 0, nbLigne, cursor));
										break;
									case "else":
										tokenList.add(new Token(TokenType.condElse, "condElse", 0, nbLigne, cursor));
										break;
									case "while":
										tokenList.add(new Token(TokenType.condWhile, "whileLoop", 0, nbLigne, cursor));
										break;
									case "for":
										tokenList.add(new Token(TokenType.condFor, "forLoop", 0, nbLigne, cursor));
										break;
									case "int":
										tokenList.add(new Token(TokenType.type, "intDeclaration", 0, nbLigne, cursor));
										break;
									case "send":
										tokenList.add(new Token(TokenType.send, "send", 0, nbLigne, cursor));
										break;
									case "receive":
										tokenList.add(new Token(TokenType.receive, "receive", 0, nbLigne, cursor));
										break;
									case "break":
										tokenList.add(new Token(TokenType.loopBreak, "break", 0, nbLigne, cursor));
										break;

									default:
										// Variable
										tokenList.add(new Token(TokenType.identificator, ident, 0, nbLigne, cursor));
										break;
									// throw new Exception(ident + " unknown (line " + nbLigne + ", col " + cursor +
									// ")");
								}
							}
						}

				}
				cursor++;
			}
			nbLigne++;
		}

		// Add last token : End of file
		tokenList.add(new Token(TokenType.EOF, "EOF", 0, nbLigne, cursor));

		return tokenList;
	}

	// Interface
	Token curent() {
		return this.tokenList.get(cursor);
	}

	void forward() {
		this.cursor++;
	}

	void accept(TokenType tokenType) throws Exception {
		if (curent().type != tokenType)
			throw new Exception("Fatal Error : line " + curent().lineNum + " Col " + curent().collum + " accept -> "
					+ curent().type + " not " + tokenType);

		forward();
	}

	boolean check(TokenType tokenType) {
		if (curent().type == tokenType) {
			forward();
			return true;
		}
		return false;
	}
}
