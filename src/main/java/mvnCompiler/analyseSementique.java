package mvnCompiler;

import java.util.Stack;

public class analyseSementique {
  int nbSlot = 0;
  Stack<symbolList> porteeSymbol;

  analyseSementique(Noeud tree) throws Exception {
    porteeSymbol = new Stack<symbolList>();
    sem(tree);
  }

  // met les annotations slot slot
  // Calcul le nb de slot d'on on a besoin : nbSLot de manière global

  void debutBLock() {
    // Cache une variable a si déja définit avant
    porteeSymbol.push(new symbolList());
  }

  void finBLock() {
    // enregistre que les varaibles déclaré dans le block ne sont plus accéssible
    porteeSymbol.pop();
    // TODO decrement nbSlot and save max needed slot (optimisation)
  }

  Symbol declarer(Noeud n) throws Exception {
    if (porteeSymbol.peek().containsKey(n.chainValue))
      throw new Exception(
          "analyseSementique : " + n.chainValue + " already declared line " + n.lineNum + " col " + n.collum);

    Symbol s = new Symbol();
    porteeSymbol.peek().put(n.chainValue, s);
    return s;
  }

  Symbol acceder(Noeud n) throws Exception {
    // regarde portée lexicale courante
    // Sinon remonte
    // Si oui renvoi
    // et ainsi de suite
    // Si fin, erreur fatale

    // porteeSymbol top to bottom
    for (int i = porteeSymbol.size() - 1; i >= 0; i--) {
      if (porteeSymbol.get(i).containsKey(n.chainValue))
        return porteeSymbol.get(i).get(n.chainValue);
    }

    throw new Exception("analyseSementique : " + n.chainValue + " not declared line " + n.lineNum + " col " + n.collum);
  }

  // 1 fnc parcour d'arbre
  void sem(Noeud n) throws Exception {
    // Ajoute les annotations dans ce Noeud
    // Noeuds ref, déclaration & block (pour la portée)

    switch (n.type) {
      case block:
        debutBLock();
        for (Noeud child : n.childrens) {
          // /!\ portée lexicale
          sem(child);
        }
        finBLock();
        break;

      case declaration:
        Symbol s = declarer(n);
        s.type = "variable";
        s.slot = nbSlot;
        nbSlot++;
        break;

      case function:
        s = declarer(n);
        s.type = "function";
        nbSlot = 0; // ???

        debutBLock();
        for (Noeud child : n.childrens)
          sem(child);
        finBLock();
        n.nbSlot = nbSlot;
        break;

      case identificator:
        s = acceder(n);
        if (s.type != "variable")
          throw new Exception(
              n.chainValue + " of type " + s.type + " is not a variable line " + n.lineNum + " col " + n.collum);

        n.slot = s.slot;
        break;

      case appel:
        s = acceder(n);
        if (s.type != "function")
          throw new Exception(
              n.chainValue + " of type " + s.type + " is not a function line " + n.lineNum + " col " + n.collum);

        for (Noeud child : n.childrens)
          sem(child);

        break;

      default:
        for (Noeud child : n.childrens)
          sem(child);
        break;
    }

    // TODO: affectation, add ref & ind, ind pour pointeurs
  }

}
