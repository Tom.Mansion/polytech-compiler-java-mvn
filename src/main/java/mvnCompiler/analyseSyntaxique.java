package mvnCompiler;

public class analyseSyntaxique {
  analyseLexicale analiseurLexical;

  // Constructor
  public analyseSyntaxique(analyseLexicale al) {
    analiseurLexical = al;
  }

  Noeud createTree() throws Exception {
    Noeud n = new Noeud(NoeudType.block, 0, 0);

    while (analiseurLexical.curent().type != TokenType.EOF) {
      addChildren(n, Function());
    }

    return n;
  }

  Noeud newNoeud(TokenType tokenType, int lineNum, int collumn) {
    NoeudType nt = null;
    switch (tokenType) {
      case addition:
        nt = NoeudType.addition;
        break;
      case incrementation:
        nt = NoeudType.incrementation;
        break;
      case soustraction:
        nt = NoeudType.soustraction;
        break;
      case decrementation:
        nt = NoeudType.decrementation;
        break;
      case multiplication:
        nt = NoeudType.multiplication;
        break;
      case division:
        nt = NoeudType.division;
        break;
      case modulo:
        nt = NoeudType.modulo;
        break;
      case puissance:
        nt = NoeudType.puissance;
        break;
      case inferieur:
        nt = NoeudType.inferieur;
        break;
      case inferieuregal:
        nt = NoeudType.inferieuregal;
        break;
      case superieur:
        nt = NoeudType.superieur;
        break;
      case superieuregal:
        nt = NoeudType.superieuregal;
        break;
      case different:
        nt = NoeudType.different;
        break;
      case egal:
        nt = NoeudType.egal;
        break;
      case et:
        nt = NoeudType.et;
        break;
      case ou:
        nt = NoeudType.ou;
        break;
      case attribution:
        nt = NoeudType.attribution;
        break;
      case parouvrante:
        nt = NoeudType.parouvrante;
        break;
      case parfermante:
        nt = NoeudType.parfermante;
        break;
      case pointvirgule:
        nt = NoeudType.pointvirgule;
        break;
      case not:
        nt = NoeudType.not;
        break;
      case condIf:
        // nt = NoeudType.condIf;
        break;
      case condFor:
        // nt = NoeudType.condFor;
        break;
      case condWhile:
        // nt = NoeudType.condWhile;
        break;
      case constant:
        nt = NoeudType.constant;
        break;
      case identificator:
        nt = NoeudType.identificator;
        break;
      case debug:
        nt = NoeudType.debug;
        break;
      case send:
        nt = NoeudType.send;
        break;
      case retour:
        nt = NoeudType.retour;
        break;
      case loopBreak:
        nt = NoeudType.loopBreak;
        break;
      default:
        break;
    }

    Noeud n = new Noeud(nt, lineNum, collumn);
    return n;
  }

  void addChildren(Noeud parent, Noeud child) {
    parent.addChildren(child);
  }

  Noeud Instruction() throws Exception {
    if (analiseurLexical.curent().type == TokenType.debug) {
      Noeud N = newNoeud(TokenType.debug, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      analiseurLexical.forward();
      Noeud E1 = expression(0);
      analiseurLexical.forward();
      analiseurLexical.check(TokenType.pointvirgule);
      addChildren(N, E1);
      return N;
    } else if (analiseurLexical.curent().type == TokenType.send) {
      Noeud N = newNoeud(TokenType.send, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      analiseurLexical.forward();
      Noeud E1 = expression(0);
      analiseurLexical.forward();
      analiseurLexical.check(TokenType.pointvirgule);
      addChildren(N, E1);
      return N;

    } else if (analiseurLexical.curent().type == TokenType.retour) {
      Noeud N = newNoeud(TokenType.retour, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      analiseurLexical.forward();
      Noeud E1 = expression(0);
      analiseurLexical.forward();
      analiseurLexical.check(TokenType.pointvirgule);
      addChildren(N, E1);
      return N;

    } else if (analiseurLexical.curent().type == TokenType.loopBreak) {
      Noeud N = newNoeud(TokenType.loopBreak, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      N.chainValue = "forcedBreak";
      analiseurLexical.forward();
      analiseurLexical.check(TokenType.pointvirgule);
      return N;

    } else if (analiseurLexical.curent().type == TokenType.accOuvrante) {
      Noeud N = new Noeud(NoeudType.block, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      analiseurLexical.forward();

      while (!analiseurLexical.check(TokenType.accFermante)) {
        addChildren(N, Instruction());
      }
      return N;

    } else if (analiseurLexical.check(TokenType.condIf)) {
      analiseurLexical.accept(TokenType.parouvrante);
      Noeud E1 = expression(0);
      analiseurLexical.accept(TokenType.parfermante);
      Noeud I1 = Instruction();

      Noeud N = new Noeud(NoeudType.test, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      addChildren(N, E1);
      addChildren(N, I1);

      if (analiseurLexical.check(TokenType.condElse)) {
        addChildren(N, Instruction());
      }
      return N;

    } else if (analiseurLexical.check(TokenType.condWhile)) {
      Noeud N = new Noeud(NoeudType.loop, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      Noeud cond = new Noeud(NoeudType.cond, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      analiseurLexical.accept(TokenType.parouvrante);
      Noeud E1 = expression(0);
      analiseurLexical.accept(TokenType.parfermante);

      Noeud I1 = Instruction();

      Noeud loopBreak = new Noeud(NoeudType.loopBreak, analiseurLexical.curent().lineNum,
          analiseurLexical.curent().collum);

      addChildren(cond, E1);
      addChildren(cond, I1);
      addChildren(cond, loopBreak);
      addChildren(N, cond);

      return N;

    } else if (analiseurLexical.check(TokenType.condFor)) {
      Noeud forBlock = new Noeud(NoeudType.block, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      analiseurLexical.accept(TokenType.parouvrante);

      Noeud E1 = expression(0);
      // TODO : check if Instructions are possible
      Noeud E1Drop = new Noeud(NoeudType.drop, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      Noeud loop = new Noeud(NoeudType.loop, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      Noeud cond = new Noeud(NoeudType.cond, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      analiseurLexical.accept(TokenType.pointvirgule);

      Noeud E2 = expression(0);

      Noeud istructionBlock = new Noeud(NoeudType.block, analiseurLexical.curent().lineNum,
          analiseurLexical.curent().collum);

      analiseurLexical.accept(TokenType.pointvirgule);

      Noeud E3 = expression(0);
      Noeud E3Drop = new Noeud(NoeudType.drop, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);

      analiseurLexical.accept(TokenType.parfermante);

      Noeud I1 = Instruction();

      addChildren(E1Drop, E1);
      addChildren(forBlock, E1Drop);
      addChildren(cond, E2);
      addChildren(E3Drop, E3);
      addChildren(istructionBlock, I1);
      addChildren(istructionBlock, E3Drop);
      addChildren(cond, istructionBlock);
      addChildren(cond,
          new Noeud(NoeudType.loopBreak, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum));
      addChildren(loop, cond);
      addChildren(forBlock, loop);

      return forBlock;

    } else if (analiseurLexical.check(TokenType.type)) {

      Token t = analiseurLexical.curent();
      analiseurLexical.accept(TokenType.identificator);
      // variable declaration
      Noeud N = new Noeud(NoeudType.declaration, t.lineNum, t.collum);
      N.chainValue = t.chainValue;
      analiseurLexical.accept(TokenType.pointvirgule);
      return N;


      // TODO : incrementation
    // } else if (analiseurLexical.check(TokenType.incrementation)) {
    //   System.out.println("analiseurLexical TokenType.incrementation ");
    //   Token t = analiseurLexical.curent();
    //   analiseurLexical.accept(TokenType.identificator);
    //   // variable declaration
    //   Noeud N = new Noeud(NoeudType.incrementation, t.lineNum, t.collum);
    //   N.chainValue = t.chainValue;
    //   return N;

    } else {
      Noeud E1 = expression(0);
      analiseurLexical.accept(TokenType.pointvirgule);
      Noeud drop = new Noeud(NoeudType.drop, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      addChildren(drop, E1);
      return drop;
    }
  }

  Noeud Function() throws Exception {
    // Function

    analiseurLexical.accept(TokenType.type);
    Token t = analiseurLexical.curent();
    analiseurLexical.accept(TokenType.identificator);
    analiseurLexical.accept(TokenType.parouvrante);

    // Funtion declaration
    Noeud n = new Noeud(NoeudType.function, t.lineNum, t.collum);
    n.chainValue = t.chainValue;

    while (analiseurLexical.curent().type != TokenType.parfermante) {
      analiseurLexical.accept(TokenType.type);

      Noeud dec = new Noeud(NoeudType.declaration, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      dec.chainValue = analiseurLexical.curent().chainValue;
      analiseurLexical.accept(TokenType.identificator);

      addChildren(n, dec);
      if (analiseurLexical.curent().type != TokenType.virgule)
        break;
      analiseurLexical.forward();
    }
    analiseurLexical.accept(TokenType.parfermante);

    Noeud functionInstructions = Instruction();
    addChildren(n, functionInstructions);

    return n;
  }

  Noeud expression(int priority) throws Exception {
    Noeud N = s();

    try {
      // TODO find a better way
      while (analiseurLexical.curent().type != TokenType.EOF && analiseurLexical.curent().type != TokenType.pointvirgule
          && analiseurLexical.curent().type != TokenType.parfermante
          && analiseurLexical.curent().type != TokenType.accFermante
          && analiseurLexical.curent().type != TokenType.virgule
          && mvnCompiler.prioriteList.list.get(analiseurLexical.curent().type).prioG >= priority) {
        TokenType op = analiseurLexical.curent().type;
        analiseurLexical.forward();

        Noeud A = expression(mvnCompiler.prioriteList.list.get(op).prioD);
        Noeud T = newNoeud(op, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
        addChildren(T, N);
        addChildren(T, A);
        N = T;
      }

    } catch (NullPointerException e) {
      throw new Exception("line " + analiseurLexical.curent().lineNum + " col " + analiseurLexical.curent().collum + " "
          + analiseurLexical.curent().type + " does not appear in mvnCompiler.prioriteList");
    }
    return N;
  }

  Noeud atome() throws Exception {
    if (analiseurLexical.check(TokenType.parouvrante)) {
      Noeud N = expression(0);
      analiseurLexical.accept(TokenType.parfermante);
      return N;

    } else if (analiseurLexical.check(TokenType.soustraction)) {
      Noeud n = new Noeud(NoeudType.soustrationU, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      Noeud arg = expression(55);
      addChildren(n, arg);
      return n;
    } else if (analiseurLexical.check(TokenType.not)) {
      Noeud n = new Noeud(NoeudType.not, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      Noeud arg = expression(55);
      addChildren(n, arg);
      return n;

    } else if (analiseurLexical.check(TokenType.multiplication)) {
      Noeud n = new Noeud(NoeudType.indirection, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      Noeud arg = expression(55);
      addChildren(n, arg);
      return n;

    } else if (analiseurLexical.curent().type == TokenType.constant) {
      Noeud n = newNoeud(TokenType.constant, analiseurLexical.curent().lineNum, analiseurLexical.curent().collum);
      n.wholeValue = analiseurLexical.curent().wholeValue;
      analiseurLexical.forward();
      return n;

    } else if (analiseurLexical.curent().type == TokenType.identificator) {
      Token t = analiseurLexical.curent();
      analiseurLexical.forward();
      if (analiseurLexical.check(TokenType.parouvrante)) {
        // Function
        Noeud n = new Noeud(NoeudType.appel, t.lineNum, t.collum);
        n.chainValue = t.chainValue;

        while (analiseurLexical.curent().type != TokenType.parfermante) {
          addChildren(n, expression(0));
          if (analiseurLexical.curent().type != TokenType.virgule)
            break;
          analiseurLexical.forward();
        }

        analiseurLexical.accept(TokenType.parfermante);
        return n;

      } else {
        // Variable
        Noeud n = newNoeud(TokenType.identificator, t.lineNum, t.collum);
        n.chainValue = t.chainValue;
        // analiseurLexical.forward();
        return n;
      }

    } else {
      throw new Exception(
          "Fatal Error : atom -> '" + analiseurLexical.curent().type + "' not '(', '-', 'const', 'ident', 'not', or 'func'");
    }
  }

  // Arrays
  Noeud s() throws Exception {
    Noeud n = atome();
    if (analiseurLexical.check(TokenType.crochetOuvrant)) {
      Noeud e = expression(0);
      analiseurLexical.accept(TokenType.crochetFermant);
      // TODO Arbre

      return e;
    } else {
      return n;
    }
  }

  // Display
  void displayTree(Noeud n, int level, boolean last) {

    for (int i = 0; i < level; i++) {
      System.out.print("│   ");
    }
    if (last) {
      System.out.print("└── ");
    } else {
      System.out.print("├── ");
    }

    // TODO change NoeudType.identificator to NoeudType.reference

    // Display value
    if (n.type == NoeudType.constant)
      System.out.println(n.wholeValue);
    else if (n.type == NoeudType.identificator)
      System.out.println(n.chainValue + " (slot : " + n.slot + ")");
    else
      System.out.println(n.type);

    for (int i = 0; i < n.childrens.size(); i++) {
      Boolean l = i == n.childrens.size() - 1;
      displayTree(n.childrens.get(i), level + 1, l);
    }

  }
}
