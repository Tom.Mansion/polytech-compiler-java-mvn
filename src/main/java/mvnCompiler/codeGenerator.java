package mvnCompiler;

import java.util.Stack;

/**
 * gencode
 */
public class codeGenerator {
    String smCode;

    Stack<Integer> stackLoop = new Stack<Integer>();
    Stack<Integer> stackIf = new Stack<Integer>();
    int nbLoop = 0;
    int nbIf = 0;
    // int nbIf = 0;

    public codeGenerator(Noeud racine) {
        // Boot strap
        smCode = ".start\n";
        smCode += "prep init\n";
        smCode += "call 0\n";
        smCode += "prep main\n";
        smCode += "call 0\n";
        smCode += "halt\n\n";

        codeGen(racine);
    }

    void codeGen(Noeud racine) {

        switch (racine.type) {
            case constant:
                smCode += "push " + racine.wholeValue + "\n";
                break;
            case addition:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "add\n";
                break;
            case soustraction:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "sub\n";
                break;
            case soustrationU:
                smCode += "push 0\n";
                codeGen(racine.childrens.get(0));
                smCode += "sub\n";
                break;
            case multiplication:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "mul\n";
                break;
            case division:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "div\n";
                break;
            case modulo:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "mod\n";
                break;
            case puissance:
                smCode += "prep puissance\n";
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "call 2\n";
                break;
            case egal:
                // cmpeq -> A == B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmpeq\n";
                break;
            case different:
                // cmpne -> A != B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmpne\n";
                break;
            case inferieur:
                // cmplt -> A < B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmplt\n";
                break;
            case inferieuregal:
                // cmple -> A <= B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmple\n";
                break;
            case superieur:
                // cmpgt -> A > B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmpgt\n";
                break;
            case superieuregal:
                // cmpge -> A >= B
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "cmpge\n";
                break;

            case et:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "and\n";
                break;
            case ou:
                codeGen(racine.childrens.get(0));
                codeGen(racine.childrens.get(1));
                smCode += "or\n";
                break;
            case not:
                codeGen(racine.childrens.get(0));
                smCode += "not\n";
                break;

            case debug:
                codeGen(racine.childrens.get(0));
                smCode += "dbg\n";
                break;
            case send:
                codeGen(racine.childrens.get(0));
                smCode += "send\n";
                break;
            case retour:
                codeGen(racine.childrens.get(0));
                smCode += "ret\n";
                break;

            case drop:
                codeGen(racine.childrens.get(0));
                smCode += "drop\n";
                break;
            case block:
                for (Noeud child : racine.childrens)
                    codeGen(child);
                break;
            case loop:
                nbLoop += 1;
                stackLoop.add(nbLoop);

                smCode += ".le" + stackLoop.peek() + "\n";

                // Cond
                codeGen(racine.childrens.get(0));
                break;

            case cond:

                // Cond type (<, >, ...)
                codeGen(racine.childrens.get(0));

                smCode += "jumpf ls" + stackLoop.peek() + "\n";
                // Block
                codeGen(racine.childrens.get(1));

                // LoopBreak
                codeGen(racine.childrens.get(2));
                break;

            case loopBreak:
                if (racine.chainValue == "forcedBreak") {
                    smCode += "jump ls" + stackLoop.peek() + "\n";
                } else {
                    smCode += "jump le" + stackLoop.peek() + "\n";
                    smCode += ".ls" + stackLoop.pop() + "\n";
                }
                break;

            case test:
                // Cond type (<, >, ...)
                nbIf += 1;
                stackIf.add(nbIf);

                // Else
                if (racine.childrens.size() == 3) {
                    codeGen(racine.childrens.get(0));

                    smCode += "jumpf ie" + stackIf.peek() + "\n";

                    // Block
                    codeGen(racine.childrens.get(1));

                    smCode += "jump is" + stackIf.peek() + "\n";

                    smCode += ".ie" + stackIf.peek() + "\n";

                    codeGen(racine.childrens.get(2));
                    smCode += ".is" + stackIf.peek() + "\n";
                } else {

                    codeGen(racine.childrens.get(0));
                    smCode += "jumpf is" + stackIf.peek() + "\n";

                    // Block
                    codeGen(racine.childrens.get(1));

                    smCode += ".is" + stackIf.peek() + "\n";
                }

                stackIf.pop();
                break;

            case attribution:
                // Affectation ?
                // if (racine.childrens.get(0).type == NoeudType.Reference) {
                // Reference (normal)
                codeGen(racine.childrens.get(1));
                smCode += "dup\n";
                smCode += "set " + racine.childrens.get(0).slot + "\n";
                break;

            // } else {
            // // Reference (pointer)
            // codeGen(racine.childrens.get(0).childrens.get(0));
            // codeGen(racine.childrens.get(1));
            // smCode += "write\n";
            // smCode += "push 0\n";
            // }

            case identificator:
                smCode += "get " + racine.slot + "\n";
                break;

            case declaration:
                break;

            // Functions
            case function:
                smCode += "." + racine.chainValue + "\n"; // Label
                smCode += "resn " + (racine.nbSlot - (racine.childrens.size() - 1)) + "\n"; //

                codeGen(racine.childrens.get(racine.childrens.size() - 1));

                smCode += "push 0\n";
                smCode += "ret\n";
                break;

            case appel:
                smCode += "prep " + racine.chainValue + "\n";
                for (Noeud child : racine.childrens)
                    codeGen(child); // expressions

                smCode += "call " + racine.childrens.size() + "\n";
                break;

            case indirection:
                // ?
                codeGen(racine.childrens.get(1));

                smCode += "call " + racine.childrens.size() + "\n";
                break;

            default:
                System.out.println("\n Error : Can't deal with node " + racine.type);
                break;
        }

    }

    // Reserve des cases mémoire
    // dès le .start

    // Pour 4 cases : push 0 4x ou resn NbSlot
}