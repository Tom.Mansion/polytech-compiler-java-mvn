package mvnCompiler;

import java.util.HashMap;

class prioriteList {

	public static HashMap<TokenType, priorite> list;

	static {
		list = new HashMap<TokenType, priorite>();
		// 65,66
		list.put(TokenType.incrementation, new priorite(65, 66, NoeudType.incrementation));

		// 60,61
		list.put(TokenType.puissance, new priorite(60, 61, NoeudType.puissance));

		// 50,51
		list.put(TokenType.multiplication, new priorite(50, 51, NoeudType.multiplication));
		list.put(TokenType.division, new priorite(50, 51, NoeudType.division));
		list.put(TokenType.modulo, new priorite(50, 51, NoeudType.modulo));

		// 40, 41
		list.put(TokenType.addition, new priorite(40, 41, NoeudType.addition));
		list.put(TokenType.soustraction, new priorite(40, 41, NoeudType.soustraction));

		// 30, 31
		list.put(TokenType.superieur, new priorite(30, 31, NoeudType.superieur));
		list.put(TokenType.superieuregal, new priorite(30, 31, NoeudType.superieuregal));
		list.put(TokenType.inferieur, new priorite(30, 31, NoeudType.inferieur));
		list.put(TokenType.inferieuregal, new priorite(30, 31, NoeudType.inferieuregal));
		list.put(TokenType.egal, new priorite(30, 31, NoeudType.egal));
		list.put(TokenType.different, new priorite(30, 31, NoeudType.different));

		list.put(TokenType.ou, new priorite(10, 11, NoeudType.ou));
		list.put(TokenType.et, new priorite(20, 21, NoeudType.et));

		list.put(TokenType.type, new priorite(5, 5, NoeudType.declaration));
		list.put(TokenType.attribution, new priorite(5, 5, NoeudType.attribution));
	}
}
